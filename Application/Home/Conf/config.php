<?php
return array(
    'URL_ROUTER_ON' => true,
    'URL_ROUTE_RULES' => array(
        array('user', 'Index/options', '', array('method' => 'options')),
        array('region', 'Index/options', '', array('method' => 'options')),
        array('goods', 'Index/options', '', array('method' => 'options')),
        array('order', 'Index/options', '', array('method' => 'options')),
        array('address', 'Index/options', '', array('method' => 'options')),
        array('arepo', 'Index/options', '', array('method' => 'options')),
        array('secondhand', 'Index/options', '', array('method' => 'options')),
        // 用户资源接口
        array('user/code/:code', 'user/checkCode', '', array('method' => 'get')),
        array('user/code', 'user/getCode', '', array('method' => 'get')),
        array('user/login', 'user/login', '', array('method' => 'post')),
        array('user/logout', 'user/logout', '', array('method' => 'get')),
        array('user/pwd/:pwd', 'user/setPwd', '', array('method' => 'get')),
        array('user/newpwd', 'user/setNewpwd', '', array('method' => 'post')),
        array('user/headimg', 'user/setHeadimg', '', array('method' => 'post')),
        array('user/name', 'user/setName', '', array('method' => 'post')),
        array('user/feedback', 'user/addFeedback', '', array('method' => 'post')),
        // array('user/cart/:id', 'user/deleteCart', '', array('method'=>'delete')),
        // array('user/cart', 'user/addToCart', '', array('method'=>'post')),
        // array('user/cart', 'user/changeNumOfGoodsInCart', '', array('method'=>'post')),
        // array('user/cart', 'user/getCart', '', array('method'=>'get')),
        //购物车接口
        array('user/cart/edit', "Cart/updateProduct", "", array('method' => 'post')),
        array('user/cart/count', "Cart/getCount", "", array('method' => 'get')),
        array('user/cart', "Cart/getProducts", "", array('method' => 'get')),
        array('user/cart', "Cart/addProduct", "", array('method' => 'post')),
        array('user/cart/:cart_id', "Cart/removeGood", "", array('method' => 'delete')),

        array('user/session', 'user/getSession', '', array('method' => 'get')),
        array('user', 'user/getDetail', '', array('method' => 'get')),

        // 地址接口
        array('address/default', 'address/getDefault', '', array('method' => 'get')),
        array('address/default', 'address/setDefault', '', array('method' => 'put')),
        array('address/:id', 'address/update', '', array('method' => 'post')),
        array('address/:id', 'address/deleteAddr', '', array('method' => 'delete')),
        array('address', 'address/add', '', array('method' => 'post')),
        array('address', 'address/getAll', '', array('method' => 'get')),

        // product_class接口
        array('product_class/first', 'ProductClass/getFirstClass', '', array('method' => 'get')),
        array('product_class/:id', 'ProductClass/getClassDetail', '', array('method' => 'get')),
        array('product_class', 'ProductClass/getClass', 'type=1', array('method' => 'get')),

        // region接口
        array('region/:region/district', 'region/getDictByRegion', '', array('method' => 'get')),
        array('region', 'region/getRegions', '', array('method' => 'get')),

        // 仓库接口
        array('arepo/:a_repository_id/homepic', 'Arepo/getHomePics', '', array('method' => 'get')),
        array('arepo/:a_repository_id/secondhand', 'SecondHand/getAllSecondHand', '', array('method' => 'get')),
        array('arepo/:a_repository_id/secondhand', 'SecondHand/addSecondHand', '', array('method' => 'post')),
        array('arepo/:a_repository_id/homead', 'Arepo/getHomeAds', '', array('method' => 'get')),
        array('arepo/:a_repository_id/sale', 'Arepo/getSales', 'num=3&page=1', array('method' => 'get')), // num是指获取几个，比如3，就是获取前三个，-1是获取全部
        array('arepo/:a_repository_id/goods', 'Arepo/getGoods', 'page=1&num=10&type=3', array('method' => 'get')), // class_id:商品分类，page：第几页，num：每页数量;type是商品分类属于第几级
        array('arepo/:a_repository_id/search', 'Arepo/searchGoods', 'page=1&num=10&type=3&key=0', array('method' => 'get')), // class_id:商品分类，page：第几页，num：每页数量;type是商品分类属于第几级

        // 商品接口
        array('goods/:product_id/sale', 'Goods/getSaleDetail', '', array('method' => 'get')),
        array('goods/:product_id/credit', 'Goods/getPerCredit', '', array('method' => 'get')),
        array('goods/:product_id/comment_count_credit', 'Goods/getCommentCountAndCredit', '', array('method' => 'get')),
        array('goods/:product_id/comment', 'Goods/getComment', 'page=1&num=10', array('method' => 'get')),
        array('goods/:product_id', 'Goods/getById', '', array('method' => 'get')),

        //订单接口
        array('order/:order_id/status', "Order/changeOrderStatus", "", array('method' => 'put')),
        array('order/:order_id/comment', "Order/addComment", "", array('method' => 'post')),
        array('order/:order_id', "Order/getOrder", "", array('method' => 'get')),
        array('order', "Order/createOrder", "", array('method' => 'post')),
        array('order', "Order/getUserOrder", "", array('method' => 'get')),

        // 二手接口
        array('secondhand/:id/comment', "SecondHand/getAllComment", "", array('method' => 'get')),
        array('secondhand/:id/comment', "SecondHand/addComment", "", array('method' => 'post')),
        array('secondhand/:id', "SecondHand/getById", "", array('method' => 'get')),
        array('secondhand/:id', "SecondHand/deleteById", "", array('method' => 'delete')),

        // 小区公告接口
        array('dist/:dist_id/info/:info_id', "Dist/getInfoById", "", array('method' => 'get')),
        array('dist/:dist_id/info', "Dist/getAllInfos", "", array('method' => 'get')),
    ),


    /* 数据缓存设置 */
    'DATA_CACHE_TIME' => 360,      // 数据缓存有效期 0表示永久缓存
    'DATA_CACHE_COMPRESS' => false,   // 数据缓存是否压缩缓存
    'DATA_CACHE_CHECK' => false,   // 数据缓存是否校验缓存
    'DATA_CACHE_PREFIX' => '',     // 缓存前缀
    'DATA_CACHE_TYPE' => 'Redis',  // 数据缓存类型,
    /*Redis设置*/
    'REDIS_HOST' => '123.57.132.201', //主机
    'REDIS_PORT' => '7936', //端口
    'REDIS_CTYPE' => 1, //连接类型 1:普通连接 2:长连接
    "REDIS_AUTH" => "xqkj",
    'REDIS_TIMEOUT' => 300, //连接超时时间(S) 0:永不超时
);
