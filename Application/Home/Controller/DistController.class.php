<?php
/**
 * Created by PhpStorm.
 * User: CatoldCui
 * Date: 2015/11/18 0018
 * Time: 0:09
 */

namespace Home\Controller;

use Home\Common\Response;
use Think\Controller\RestController;
class DistController extends RestController
{
    // 获取小区下所有公告
    public function getAllInfos()
    {
        $response = new Response();

        $dist_id = I('dist_id');
        $result = M('district_notification')->field('district_notification_id, title, createtime')
            ->where('district_id = %d', $dist_id)->order('"index" desc')->select();

        $response->addData('infos', $result);
        $this->response($response, 'json');
    }

    // 获取有公告详情
    public function getInfoById()
    {
        $response = new Response();

        $info_id = I('info_id');

        $result = M('district_notification')->find($info_id);

        $response->addData('info', $result);
        $this->response($response, 'json');
    }
}