<?php
/**
 * Created by PhpStorm.
 * User: YanZ
 * Date: 15/11/12
 * Time: 下午1:35
 */

namespace Home\Controller;

use Home\Common\BizCode;
use Home\Common\Response;
use Think\Controller\RestController;
use Think\Exception;

class SecondHandController extends RestController
{
    private $user;

    public function _initialize()
    {
        $this->user = session('user');

        if(!IS_GET){
          if (!$this->user) {
              $response = new Response();
              $response->setFailState('UN_LOGIN');
              $response->addData('user', $user);
              $this->response($response, 'json');
              exit;
          }
        }
    }

    public function addSecondHand()
    {
        $response = new Response();

        $savePath = 'secondhand';
        $realPath = __ROOT__ . '/Public/upload/' . $savePath . '/';

        //如果目录不存在，创建目录
        if (!file_exists(DOC_ROOT . '/Public/upload/' . $savePath . '/')) {
            mkdir(DOC_ROOT . '/Public/upload/' . $savePath . '/');
        }

        $config = array(
            'rootPath' => DOC_ROOT . '/Public/upload/',
            'savePath' => '/' . $savePath . '/',
            'saveName' => array('uniqid', ''),
            'exts' => array('jpg', 'gif', 'png', 'jpeg'),
            'autoSub' => false,
            'subName' => array('date', 'Ymd'),
        );
        $upload = new \Think\Upload($config);// 实例化上传类

        if(!$_FILES){
          $data['a_repository_id'] = I('get.a_repository_id');
          $data['content'] = I('content');
          $data['user_id'] = $this->user['user_id'];

          M('secondhand_product')->add($data);
          $this->response($response, 'json');
          exit;
        }
        
        // 上传文件
        $info = $upload->upload();
        if (!$info) {// 上传错误提示错误信息
            $response->setFailState('IMG_UPLOAD_ERROR');
        } else {// 上传成功
            // echo json_encode($info['file']);
            for($i = 0; $i < count($info) && $i < 3; $i++){
              $file = $info[$i];
              $imgtype = 'imgurl'.($i + 1);
              $data[$imgtype] = $realPath.$file['savename'] ;
            }

            $data['a_repository_id'] = I('get.a_repository_id');
            $data['content'] = I('content');
            $data['user_id'] = $this->user['user_id'];

            M('secondhand_product')->add($data);
        }

        // $response->addData('info', I('param.'));
        $this->response($response, 'json');
    }

    public function getAllSecondHand()
    {
        $response = new Response();

        $dist_id = I('a_repository_id');
        $result = D('SecondhandProduct')->relation('user')->where('a_repository_id = %d and status=1', $dist_id)->order('createtime desc')->select();

        $response->addData('seconds', $result);
        $this->response($response, 'json');
    }


    public function getAllComment()
    {
        $response = new Response();
        $id = I('id');

        $result = D('SecondhandComment')->relation(array('from_user', 'to_user'))->where('secondhand_product_id = %d', $id)->select();
        $response->addData('comments', $result);
        $this->response($response, 'json');
    }

    public function addComment()
    {
        $response = new Response();

        $comment = I('comment');
        $comment['from_user_id'] = $this->getUserId();
        $comment['secondhand_product_id'] = I('id');
        $comment['secondhand_comment_id'] = M('secondhand_comment')->add($comment);
        $response->addData('comment', $comment);

        $this->response($response, 'json');
    }

    public function getById()
    {
        $response = new Response();

        $id = I('id');

        $comments = D('SecondhandComment')->relation(array('from_user', 'to_user'))->where('secondhand_product_id = %d', $id)->select();

        $second = D('SecondhandProduct')->relation('user')->find($id);

        $response->addData('comments', $comments);
        $response->addData('second', $second);

        $this->response($response, 'json');
    }

    public function deleteById()
    {
        $response = new Response();

        $this->response($response, 'json');
    }

    //获取当前用户id
    private function getUserId()
    {
        return isset($this->user) ? $this->user['user_id'] : 0;
    }
}
