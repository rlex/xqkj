<?php
namespace Home\Controller;
use Think\Controller;
use Think\Controller\RestController;
use Home\Common\Response;
use Home\Common\BizCode;
use Home\Common\ConditionBuilder;
class ProductClassController extends RestController {
  public function getClass()
  {
    $response = new Response();
    $parent_id = I('parent_id');
    if(!empty($parent_id)){
      $condition['parent_id'] = $parent_id;
    }

    $condition['type'] = I('type');
    $Dao = D('ProductClass');
    $result = $Dao->relation(true)->where($condition)->order('product_class.index')->select();

    $response->addData('classes', $result);

    $this->response($response, 'json');
  }

  public function getFirstClass(){
    $response = new Response();

    $condition['type'] = 1;
    $Dao = M('product_class');
    $result = $Dao->where($condition)->order('product_class.index')->select();

    $response->addData('classes', $result);
    $this->response($response, 'json');
  }

  public function getClassDetail()
  {
    $response = new Response();

    $condition['product_class_id'] = I('get.id');
    $Dao = M('product_class');
    $result = $Dao->where($condition)->order('product_class.index')->find();

    $response->addData('class', $result);
    $this->response($response, 'json');
  }
}
