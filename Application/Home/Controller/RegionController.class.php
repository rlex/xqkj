<?php
namespace Home\Controller;
use Think\Controller;
use Think\Controller\RestController;
use Home\Common\Response;
use Home\Common\BizCode;
use Home\Common\ConditionBuilder;
use Home\Common\Utils;
class RegionController extends RestController {
  public function getRegions() {
    $response = new Response();

    $Dao = M('district');
    $result = $Dao->field('region')->group('region')->select();
    $temp = array();
    foreach ($result as &$value) {
      array_push($temp, $value['region']);
    }

    $response->addData('regions', $temp);

    $this->response($response, 'json');
  }

  public function getDictByRegion() {
    $response = new Response();

    $condition['region'] = I('region');
    $Dao = M('district');
    $result = $Dao->where($condition)->select();

    $temp = array();
    foreach ($result as &$value) {
      $value['first'] = Utils::getFirstCharter($value['name']);
      array_push($temp, $value);
    }

    usort($temp, function($a, $b){
      return $a['first'] - $b['first'];
    });

    $response->addData('districts', $temp);

    $this->response($response, 'json');
  }
}
