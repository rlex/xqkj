<?php
namespace Home\Controller;

use Think\Controller;
use Think\Controller\RestController;
use Home\Common\Response;
use Home\Common\BizCode;
use Home\Common\ConditionBuilder;

class ArepoController extends RestController
{
    public function getHomePics()
    {
        $response = new Response();

        $Dao = M('home_picture');
        $condition['a_repository_id'] = I('a_repository_id');
        $result = $Dao->where($condition)->order('position')->select();
        $response->addData('homepics', $result);

        $this->response($response, 'json');
    }

    public function getHomeAds()
    {
        $response = new Response();

        $Dao = D('HomeAdvertisement');
        $condition['a_repository_id'] = I('a_repository_id');
        $result = $Dao->relation(true)->where($condition)->order('position')->select();
        $response->addData('homeads', $result);

        $this->response($response, 'json');
    }

    public function getSales()
    {
        $response = new Response();

        $a_repository_id = I('a_repository_id');
        $num = I('num') < 1 ? 1 : I('num');
        $page = I('page') < 1 ? 1 : I('page');
        $class_type = (int)I('type');

        $class_id = I('class_id');
        // 判断是否按class_id查询
        if (empty($class_id)) {
            $isCheckClass = 1;
        }
        if ($class_type < 1 || $class_type > 3) {
            $class_type = 3;
        }

        $class_type = 'class' . $class_type;
        $bid = D("ARepository")->find($a_repository_id)['b_repository_id'];

        $Dao = D('Product');
        // $result = $Dao->relation(true)->where('(a_repository_id = %d or b_repository_id = %d)', $a_repository_id, $bid, $class_id)->page($page, $num)->select();
        $result = $Dao->field('*, product.status as product_status, product.default_amount as amount, product.kj_price as kj_price')
            ->join('product_template on product.product_template_id = product_template.product_template_id')
            ->join('product_discount on product_discount.product_id = product.product_id')
            ->where('(a_repository_id = %d or b_repository_id = %d) and (%d = 1 or %s = "%d") and product.status = "0"', $a_repository_id, $bid, $isCheckClass, $class_type, $class_id)->order('product_discount.index')->page($page, $num)->select();

        // var_dump($Dao);
        foreach ($result as &$value) {
            $value['product_detail'] = '';
        }
        $response->addData('products', $result);
        $this->response($response, 'json');
    }

    public function getGoods()
    {
        $response = new Response();

        $a_repository_id = I('a_repository_id');
        $num = I('num') < 1 ? 1 : I('num');
        $page = I('page') < 1 ? 1 : I('page');
        $class_type = (int)I('type');
        if ($class_type < 1 || $class_type > 3) {
            $class_type = 3;
        }

        $class_type = 'class' . $class_type;
        $class_id = I('class_id');
        // 当class_id 为0时不分类别
        $isAll = 0;
        if(empty($class_id) || $class_id == '0'){
          $isAll = 1;
        }
        $bid = D("ARepository")->find($a_repository_id)['b_repository_id'];

        $Dao = D('Product');
        // $result = $Dao->relation(true)->where('(a_repository_id = %d or b_repository_id = %d)', $a_repository_id, $bid, $class_id)->page($page, $num)->select();
        $result = $Dao->field('*, product.status as product_status, product.default_amount as amount, product.kj_price as kj_price, product.index as "index"')->join('product_template on product.product_template_id = product_template.product_template_id')->where('(a_repository_id = %d or b_repository_id = %d) and (1 = %d or %s = %d) and product.status = "0"', $a_repository_id, $bid, $isAll, $class_type, $class_id)->order('product.index')->page($page, $num)->select();

        foreach ($result as &$value) {
            $value['product_detail'] = '';
        }
        $response->addData('products', $result);
        $this->response($response, 'json');
    }

    public function searchGoods()
    {
        $response = new Response();

        $a_repository_id = I('a_repository_id');
        $key = I('key');
        $num = I('num') < 1 ? 1 : I('num');
        $page = I('page') < 1 ? 1 : I('page');
        $class_type = (int)I('type');
        if ($class_type < 1 || $class_type > 3) {
            $class_type = 3;
        }

        $class_type = 'class' . $class_type;
        $class_id = I('class_id');
        // 当class_id 为0时不分类别
        $isAll = 0;
        if(empty($class_id) || $class_id == '0'){
          $isAll = 1;
        }
        $bid = D("ARepository")->find($a_repository_id)['b_repository_id'];

        $Dao = D('Product');
        // $result = $Dao->relation(true)->where('(a_repository_id = %d or b_repository_id = %d)', $a_repository_id, $bid, $class_id)->page($page, $num)->select();
        $result = $Dao->field('*, product.status as product_status, product.default_amount as amount, product.kj_price as kj_price, product.index as "index"')->join('product_template on product.product_template_id = product_template.product_template_id and product_template.key like "%'.$key.'%"')->where('(a_repository_id = %d or b_repository_id = %d) and (1 = %d or class1 = %d or class2 = %d or class3 = %d) and product.status = "0"', $a_repository_id, $bid, $isAll, $class_id, $class_id, $class_id)->order('product.index')->page($page, $num)->select();

        foreach ($result as &$value) {
            $value['product_detail'] = '';
        }
        $response->addData('products', $result);
//        $response->addData('data', I());
        // var_dump($Dao);
        $this->response($response, 'json');
    }
}
