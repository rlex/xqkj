<?php
/**
 * Created by PhpStorm.
 * User: YanZ
 * Date: 15/11/12
 * Time: 下午1:41
 */

namespace Home\Model;


use Think\Model\RelationModel;

class OrderModel extends RelationModel
{
    protected $tableName = "order";

    protected $_link = array(
        'order_products' => array(
            'class_name' => 'order_product',
            'mapping_type' => self::HAS_MANY,
            'foreign_key' => 'order_id',
            'mapping_name' => 'order_products'
        )
    );

}