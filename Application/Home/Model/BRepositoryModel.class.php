<?php
/**
 * Created by PhpStorm.
 * User: clay
 * Date: 2015/11/2
 * Time: 2:12
 */

namespace Home\Model;


use Think\Model\RelationModel;

class BRepositoryModel extends RelationModel
{

    protected $tableName = "b_repository";

    protected  $_link = array(
        'a_repository' => array(
            'mapping_type' => self::HAS_MANY,
            'foreign_key'  => 'b_repository_id',
            'mapping_name' => 'a_repository',
        ),
        'products' => array(
          'mapping_type' => self::HAS_MANY,
          'class_name' => 'product',
          'foreign_key'  => 'b_repository_id',
        )
    );

}
