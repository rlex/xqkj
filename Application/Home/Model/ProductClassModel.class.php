<?php

namespace Home\Model;
use Think\Model\RelationModel;

class ProductClassModel extends RelationModel
{

    protected $tableName = "product_class";

    protected  $_link = array(
        'sub_class' => array(
          'class_name' => 'ProductClass',
          'mapping_type' => self::HAS_MANY,
          'foreign_key'  => 'product_class_id',
          'parent_key' => 'parent_id',  // 关联表外键
          'mapping_name' => 'sub_class',
          'mapping_order' => 'product_class.index'
        )
    );

}
