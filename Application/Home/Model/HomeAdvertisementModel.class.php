<?php

namespace Home\Model;
use Think\Model\RelationModel;

class HomeAdvertisementModel extends RelationModel
{

    protected $tableName = "home_advertisement";

    protected  $_link = array(
        'product_class' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'product_class_id',
            'mapping_name' => 'product_class'
        )
    );
}
