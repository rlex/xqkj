<?php

namespace Home\Model;
use Think\Model\RelationModel;

class ProductModel extends RelationModel
{

    protected $tableName = "product";

		protected  $_link = array(
        'product_template' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'product_template_id',
            'mapping_name' => 'product_template',
            'class_name' => 'ProductTemplate'
        )
    );
}
