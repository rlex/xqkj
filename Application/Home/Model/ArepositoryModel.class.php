<?php

namespace Home\Model;
use Think\Model\RelationModel;

class ArepositoryModel extends RelationModel
{

    protected $tableName = "a_repository";

    protected  $_link = array(
        'b_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'b_repository_id',
            'mapping_name' => 'b_repository',
            'as_fields' => 'name:b_name, position:b_position, status:b_status'
        ),
        'products' => array(
          'mapping_type' => self::HAS_MANY,
          'class_name' => 'product',
          'foreign_key'  => 'a_repository_id'
        )
    );

    public static function b_repository_for($id){
        return (new ARepositoryModel())->relation(true)->find($id);
    }
}
