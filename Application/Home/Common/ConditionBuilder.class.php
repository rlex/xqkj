<?php
namespace Home\Common;

class ConditionBuilder {
 public static function createCondition($keys){
   $condition = array();
   foreach ($keys as $key => $value) {
     $condition[$value] = I($value);
   }
   return $condition;
 }
}
 ?>
