<?php
namespace Home\Common;
class Response {
  public $state;
  public $data = array();

  public function __construct()
  {
    if(!IS_GET){
      header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
    } else {
      header('Access-Control-Allow-Origin: http://localhost');
    }
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: *');
    header('Access-Control-Allow-Credentials: true');
    $this->state =  BizCode::STATE('SUCCESS');
  }

  public function setFailState($state){
    $this->state = BizCode::STATE($state);
  }

  public function addData($key, $value){
    $this->data[$key] = $value;
  }
}
