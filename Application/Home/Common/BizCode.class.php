<?php
namespace Home\Common;
/**
 *
 */
 class BizCode {
    private static $consts = array(
        'SUCCESS' => array("code" => '200', 'message' => '操作成功'),

        //　全局错误
        'PARAM_INVALID' => array("code" => '101', 'message' => '参数错误'),
        'UN_LOGIN' => array("code" => '102', 'message' => '未登录'),
        'OPERATE_DATABASE_ERROR' => array("code" => '103', 'message' => '操作数据库失败'),
        'IMG_UPLOAD_ERROR' => array("code" => '104', 'message' => '图片上传失败'),

        'USER_LOGIN_FAIL'=> array("code" => '301', 'message' => '用户名或密码错误'),
        'USER_CODE_CHECK_FAIL'=> array("code" => '302', 'message' => '验证码错误'),
        'USER_SET_PWD_FAIL'=> array("code" => '303', 'message' => '用户设置密码失败'),
        'USER_PHONE_EXISTED'=> array("code" => '304', 'message' => '用户手机已存在'),
        'USER_PHONE_NOT_EXIST'=> array("code" => '305', 'message' => '手机号不存在'),
        'USER_CODE_SEND_ERROR'=> array("code" => '305', 'message' => '验证码发送失败'),
        'USER_OLDPWD_ERROR'=> array("code" => '307', 'message' => '旧密码不正确'),

        'ADDRESS_NOT_EXIST_ERROR'=> array("code" => '405', 'message' => '地址不存在'),

        'ORDER_GOODS_NUM_NOT_ENOUGH'=> array("code" => '501', 'message' => '商品数量不足'),
        'ORDER_GOODS_DISCOUNT_LIMIT'=> array("code" => '502', 'message' => '已经达到限购数量'),
        'ORDER_COMMENT_ERROR'=> array("code" => '503', 'message' => '订单评价失败'),
        'ORDER_CART_NOT_EXIST'=> array("code" => '505', 'message' => '购物车商品已不存在'),
        'ORDER_TIMEOUT'=> array("code" => '504', 'message' => '目前暂停营业<br>营业时间:9:00-21:00'),

        'GOODS_NOT_ON_SALE' => array('code' => '601', 'message' => '商品不是促销商品'),

    );

    public static function STATE($name) {
        return self::$consts[$name];
    }
}
