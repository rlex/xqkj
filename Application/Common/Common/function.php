<?php
//文件上传

function saveFile($savePath)
{
    //如果目录不存在，创建目录
    if (!file_exists(DOC_ROOT . '/Public/upload/' . $savePath . '/')) {
        mkdir(DOC_ROOT . '/Public/upload/' . $savePath . '/');
    }

    $config = array(
        //'maxSize'    =>    3145728,
        'rootPath' => DOC_ROOT . '/Public/upload/',
        'savePath' => '/' . $savePath . '/',
        'saveName' => array('uniqid', ''),
        'exts' => array('jpg', 'gif', 'png', 'jpeg'),
        'autoSub' => false,
        'subName' => array('date', 'Ymd'),
    );
    $upload = new \Think\Upload($config);// 实例化上传类

    // 上传文件
    $info = $upload->upload();
    if (!$info) {// 上传错误提示错误信息
        echo $upload->getError();
    } else {// 上传成功
        echo json_encode($info['file']);
    }
}

?>
