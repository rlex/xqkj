<?php
namespace Admin\Controller;
use Think\Controller;
class OrderController extends Controller {
    public function _initialize(){
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }
    
    public function index(){
        $this->display('index');
    }

    public function orderList(){
		$this->display('Order/order-list');
    }

    public function orderDetail(){
    	$orderId = I('param.orderId');
    	$this->display('Order/order-detail');
    }

    public function deliverList(){
    	$this->display('Order/deliver-list');
    }

    public function deliverDetail(){
    	$this->display('Order/deliver-detail');
    }

    public function returnList(){
    	$this->display('Order/return-list');
    }

    public function returnDetail(){
    	$this->display('Order/return-detail');
    }

    public function getOrder(){
        $order_id = I('param.order_id', '');

        //仓库管理员只可以看到自己所属的仓库的订单列表
        if(session('admin')['a_repository_id'] != null && session('admin')['a_repository_id'] != '0'){
            $param['a_repository_id'] = session('admin')['a_repository_id'];
        }else if(session('admin')['b_repository_id'] != null && session('admin')['b_repository_id'] != '0'){
            $param['b_repository_id'] = session('admin')['b_repository_id'];
        }

        $Model = D('order');
        if($order_id == ''){
            $r['data'] = $Model->where($param)->relation('user')->select();
        }else{
            $param['order_id'] = $order_id;
            $r = $Model->where($param)->relation(true)->find();
        }

        echo json_encode($r);
    }

    public function modOrderStatus(){
        $data = I('param.');

        //先修改订单状态，将最后一次的操作的管理员姓名存储在订单表里
        $data['admin_name'] = session('admin')['name'];
        $Model = M('order');
        $r1 = $Model->save($data);

        //如果确认付款，修改paytime为当前时间
        $param['order_id'] = $data['order_id'];
        if($data['status'] == '1'){
            $r1 = $Model->where($param)->setField('paytime', date("Y-m-d H:i:s",time()));
        }

        //在订单日志表里添加管理员操作记录
        $Model = M('admin_order');
        $data['admin_id'] = session('admin')['admin_id'];
        $r2 = $Model->add($data);

        //用户积分添加（商品钱数等于积分数，打折商品不参与积分）
        $Model = M('order_product');
        $products = $Model->where($param)->select();
        $credit = 0;
        foreach ($products as &$pro) {
            if($pro['product_discount_id'] == null){
                $credit += $pro['realprice'];
            }
        }
        $order = M('order')->where($param)->find();
        $user['user_id'] = $order['user_id'];
        $r3 = M('user')->where($user)->setInc('credit', $credit);

        echo json_encode($r2);
    }
    
    //退货时修改订单商品状态，再修改订单状态，再插入到操作记录中
    public function modProductStatus(){
        $data = I('param.');
        $Model = M('order_product');
        $r1 = $Model->save($data);

        //如果有一个商品已退货，则订单状态置为已退货，将最后一次的操作的管理员姓名存储在订单表里
        // $param['order_id'] = I('param.order_id');
        // $param['status'] = '0';
        //$product = $Model->where($param)->select();
        //有一个商品已退货，则订单状态为已退货
        $r2['order_id'] = $data['order_id'];
        $r2['status'] = '4';
        $r2['admin_name'] = session('admin')['name'];
        $Model = M('order');
        $order = $Model->field('return_serial_id')->find($data['order_id']);
        if(is_null($order['return_serial_id'])){
            $r2['return_serial_id'] = uniqid(date("Ymd"));
        }else{
            $r2['return_serial_id'] = $order['return_serial_id'];
        }
        $r3 = $Model->save($r2);

        //将退回商品数量添加到库存里
        $p['order_product_id'] = $data['order_product_id'];
        $product = M('order_product')->field('product_id')->where($p)->find();
        $r4 = M('product')->where($product)->setInc('default_amount', $data['return_num']);

        //在订单日志表里添加管理员操作记录
        $Model = M('admin_order');
        $r2['admin_id'] = session('admin')['admin_id'];
        $r2['order_id'] = $data['order_id'];
        $r2['status'] = '4';
        $r5 = $Model->add($r2);
        echo json_encode($r4);
    }

    public function getDeliver(){
        $order_id = I('param.order_id', '');

        //订单状态为已发货"0"
        $param['status'] = '0';
        //仓库管理员只可以看到自己所属的仓库的发货单列表
        if(session('admin')['a_repository_id'] != null && session('admin')['a_repository_id'] != '0'){
            $param['a_repository_id'] = session('admin')['a_repository_id'];
        }else if(session('admin')['b_repository_id'] != null && session('admin')['b_repository_id'] != '0'){
            $param['b_repository_id'] = session('admin')['b_repository_id'];
        }
        
        $Model = D('order');
        if($order_id == ''){
            $r['data'] = $Model->relation('products')->where($param)->select();
            $this->getDeliverRepository($r['data']);
            
        }else{
            $param['order_id'] = $order_id;
            $r = $Model->where($param)->relation(true)->find();
        }

        echo json_encode($r);
    }

    public function getReturn(){
        $order_id = I('param.order_id', '');

        //订单状态为已退货"4"
        $param['status'] = '4';
        //仓库管理员只可以看到自己所属的仓库的退货单列表
        if(session('admin')['a_repository_id'] != null && session('admin')['a_repository_id'] != '0'){
            $param['a_repository_id'] = session('admin')['a_repository_id'];
        }else if(session('admin')['b_repository_id'] != null && session('admin')['b_repository_id'] != '0'){
            $param['b_repository_id'] = session('admin')['b_repository_id'];
        }
        
        $Model = D('order');
        if($order_id == ''){
            $r['data'] = $Model->relation('products')->where($param)->select();
            $this->getDeliverRepository($r['data']);
        }else{
            $param['order_id'] = $order_id;
            $r = $Model->where($param)->relation(true)->find();
        }

        echo json_encode($r);
    }

    //对每个订单，找出商品对应仓库名字
    private function getDeliverRepository(&$r){ 
        foreach ($r as &$order){
            unset($a_repository_id);
            unset($b_repository_id);
            foreach ($order['products'] as &$product) {
                if($product['a_repository_id'] != null && $product['a_repository_id'] != 0){
                    $a_repository_id = $product['a_repository_id'];
                }
                if($product['b_repository_id'] != null && $product['b_repository_id'] != 0){
                    $b_repository_id = $product['b_repository_id'];
                }
            }
            unset($order['products']);
            //对每个ab仓库id，找出ab仓库 name
            if(isset($a_repository_id)){
                $A = M('a_repository');
                $a_repository_name = $A->find($a_repository_id);
                $order['a_repository_name'] = $a_repository_name['name'];
            }
            if(isset($b_repository_id)){
                $B = M('b_repository');
                $b_repository_name = $B->find($b_repository_id);
                $order['b_repository_name'] = $b_repository_name['name'];
            }
        }
    }

    //对已打印的订单设置已打印操作
    public function updatePrint(){
        $param['order_id'] = I('param.order_id');
        $Model = M("order");
        $r = $Model->where($param)->setField("isprint", "1");
        echo json_encode($r);
    }
}