<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Model;
class SecondhandController extends Controller {
    public function _initialize(){
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }

    public function secondhandProduct(){
        $this->display('Secondhand/secondhand-product');
    }

    public function secondhandComment(){
        $this->display('Secondhand/secondhand-comment');
    }

    public function getSecondhandProduct(){
        $data['secondhand_product_id'] = I('param.secondhand_product_id', '');
        $Model = D('secondhand_product');
        if($data['secondhand_product_id'] == ''){
            $r['data'] = $Model->relation('user')->select();
        }else{
            $r = $Model->where($data)->relation('user')->find();
        }
        
        echo json_encode($r);
    }

    public function updateSecondhandProduct(){
        $data = I('param.');
        $Model = M('secondhand_product');
        $r = $Model->save($data);
        echo json_encode($r);
    }

    public function deleteSecondhandProduct(){
        $param['secondhand_product_id'] = I('param.secondhand_product_id');

        $r = M('secondhand_product')->where($param)->delete();
        $r = M('secondhand_comment')->where($param)->delete();
        echo json_encode($r);
    }

    public function getSecondhandComment(){
        $Model = D('SecondhandComment');
        $r['data'] = $Model->relation(array('from_user', 'to_user', 'secondhand_product'))->select(); 
        echo json_encode($r);
    }

    public function updateSecondhandComment(){
        $data = I('param.');
        $Model = M('secondhand_comment');
        $r = $Model->save($data);
        echo json_encode($r);
    }

    public function deleteSecondhandComment(){
        $param['secondhand_comment_id'] = I('param.secondhand_comment_id');
        $Model = M('secondhand_comment');
        $r = $Model->where($param)->delete();
        echo json_encode($r);
    }

}