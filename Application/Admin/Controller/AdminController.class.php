<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Model;
class AdminController extends Controller {
    public function _initialize(){
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }

    public function adminList(){
       $this->display('Admin/admin-list');
    }

    public function adminCredit(){
        $this->display('Admin/admin-credit');
    }
    
    public function getAdmin(){
        $data['admin_id'] = I('param.admin_id', '');
        $Model = D('admin');
        if($data['admin_id'] == ''){
            $r['data'] = $Model->relation(true)->select();
        }else{
            $r = $Model->where($data)->find();
        }
        
        echo json_encode($r);
    }

    public function updateAdmin(){
        $data = I('param.');
        $Model = M('admin');
        if($data['admin_id'] != ''){
            $r = $Model->save($data);
        }else{
            $r = $Model->add($data);
        }
        
        echo json_encode($r);
    }

    public function deleteAdmin(){
        $data['admin_id'] = I('param.admin_id', '');
        $Model = M('admin');
        if($data['admin_id'] != ''){
            $r = $Model->where($data)->delete();
        }
        echo json_encode($r);
    }

    public function getAdminCredit(){
        $Model = D('admin_credit');
        $r['data'] = $Model->relation(true)->select();
        echo json_encode($r);
    } 

}