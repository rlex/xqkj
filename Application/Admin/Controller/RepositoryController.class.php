<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Model;
class RepositoryController extends Controller {
    public function _initialize(){
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }

    public function aRepositoryDetail(){
			$this->display('Repository/a-repository-detail');
    }

    public function aRepositoryList(){
    	$this->display('Repository/a-repository-list');
    }

    public function bRepositoryDetail(){
			$this->display('Repository/b-repository-detail');
    }

    public function bRepositoryList(){
    	$this->display('Repository/b-repository-list');
    }

    public function repositoryWarn(){
        $this->display('Repository/repository-warning');
    }

    public function getBRepository(){
        $param['b_repository_id'] = I('param.b_repository_id', '');

        $Model = M('b_repository');
        if($param['b_repository_id'] == ''){
            //仓库管理员不能看到仓库列表
            $r['data'] = '';
            if(session('admin')['type'] != '0'){
                $r['data'] = $Model->select();
            }
        }else{
            $r = $Model->where($param)->find();
        }

        echo json_encode($r);
    }

    public function updateBRepository(){
        $data = I('param.');

        $Model = M('b_repository');
        if($data['b_repository_id'] != ''){
            $r = $Model->save($data);
        }else{
            $r = $Model->add($data);
        }
        echo json_encode($r);
    }

    public function deleteBRepository(){
        $param['b_repository_id'] = I('param.b_repository_id', '');
        $Model = M('b_repository');
        $r = $Model->where($param)->delete();
        echo json_encode($r);
    }

    public function getARepository(){
        $param['a_repository_id'] = I('param.a_repository_id', '');

        $Model = M('a_repository');
        if($param['a_repository_id'] == ''){
            //仓库管理员不能看到仓库列表
            $r['data'] = '';
            if(session('admin')['type'] != '0'){
                $r['data'] = $Model->select();
            }
        }else{
            $r = $Model->where($param)->find();
        }

        echo json_encode($r);
    }

    public function updateARepository(){
        $data = I('param.');

        $Model = M('a_repository');
        if($data['a_repository_id'] != ''){
            $r = $Model->save($data);
        }else{
            $r = $Model->add($data);
        }
        echo json_encode($r);
    }

    public function deleteARepository(){
        $param['a_repository_id'] = I('param.a_repository_id', '');
        $Model = M('a_repository');
        $r = $Model->where($param)->delete();
        echo json_encode($r);
    }

    public function getRepository(){
        $a = M('a_repository');
        $r1 = $a->field('b_repository_id, a_repository_id, name')->select();

        $b = M('b_repository');
        $r2 = $b->field('b_repository_id, name')->select();

        $r = array_merge($r1, $r2);
        echo json_encode($r);
    }

    //获取库存报警的商品列表
    public function getRepositoryWarn(){
        $Model = D('product');
        $r['data'] = $Model->where('default_amount < warning_amount')->relation(array('a_repository', 'b_repository'))->select();
        echo json_encode($r);
    }

}