<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {
    
    public function index(){
        $this->display('Index/index');
    }
    public function test(){
    	$this->display('Index/test');
    }

    public function login(){
    	$this->display('Index/login');
    }

    public function doLogin(){
        $data['name'] = I('param.name');
        $data['pwd'] = I('param.pwd');

        $Model = M('admin');
        $r = $Model->where($data)->find();
        if(count($r) > 0 && $r['status'] == '0'){
            session('admin', $r);
            $result['result'] = '0';
        }else{
            if(count($r) == 0){
                $result['result'] = '1';
            }else{
                $result['result'] = '2';
            }
        }
        echo json_encode($result);
    }

    public function doLogout(){
        unset($_SESSION['admin']);
        $this->display('Index/login');
    }

    //ab仓库需要补货以及需要补货数量的数据
    public function getABRepositorySupply(){
        $Model = M('product');
        $result = $Model->field('product.product_id, product.name, product_template.barcode, product.kj_price, (product.origin_amount-product.default_amount) as supply_amount')
                    ->join('product_template on product.product_template_id=product_template.product_template_id')
                    ->where('product.default_amount<product.origin_amount')
                    ->select();
        $Model = M('statistic_abrepository_supply');
        $Model->startTrans();
        foreach ($result as &$supply) {
            $r = $Model->add($supply);
            if(!$r){
                break;
            }
        }
        if(!$r){
            $Model->rollback();
            echo "false";
        }else{
            $Model->commit();
            echo json_encode($result);
        }
    }

    //统计AB仓库销售单
    public function getABRepositorySale(){
        $Model = M('order_product');
        $result = $Model->field('order_product.product_id, order_product.name, order_product.barcode, order_product.realprice as real_price, product_template.jh_price, order_product.num as sale_amount, product.default_amount as remain_amount')
                ->join('`order` o on o.order_id=order_product.order_id and (o.status="1" or o.status="2" or o.status="3" or o.status="4")')
                ->join('product on order_product.product_id=product.product_id')
                ->join('product_template on product.product_template_id=product_template.product_template_id')
                ->where('UNIX_TIMESTAMP(o.paytime)>UNIX_TIMESTAMP(curdate()) and UNIX_TIMESTAMP(o.paytime)<UNIX_TIMESTAMP(date_add(curdate(),interval 1 day))')
                ->select();
        //遍历取出的结果，把同一仓库卖出的商品汇总
        $temp = array(); //存储不重复的商品
        foreach ($result as &$r) {
            $isRepeat = false;
            foreach ($temp as &$t) {
                if($t['product_id'] == $r['product_id'] && $t['realprice'] == $r['realprice']){
                    $t['sale_amount'] += $r['sale_amount'];
                    $isRepeat = true;
                    break;
                }
            }
            if(!$isRepeat){
                array_push($temp, $r);
            }
        }
        $result = $temp;
        $Model = M('statistic_abrepository_sale');
        $Model->startTrans();
        foreach ($result as &$sale) {
            $r = $Model->add($sale);
            if(!$r){
                break;
            }
        }
        if(!$r){
            $Model->rollback();
            echo "false";
        }else{
            $Model->commit();
            echo json_encode($result);
        }
    }

    // 统计B仓库需要送货
    public function getBRepositoryDeliver(){
        $Model = M('order_product');
        $result = $Model
            ->field('order_product.product_id, order_product.name, order_product.barcode, order_product.num as deliver_amount, o.a_repository_id, o.b_repository_id')
            ->join('`order` o on o.order_id=order_product.order_id')
            ->where('UNIX_TIMESTAMP(order_product.createtime)>UNIX_TIMESTAMP(curdate()) and UNIX_TIMESTAMP(order_product.createtime)<UNIX_TIMESTAMP(date_add(curdate(),interval 1 day)) and ISNULL(order_product.a_repository_id)')
            ->select();
        //遍历取出的结果，把需要送到同一A仓库的商品汇总
        $temp = array(); //存储不重复的商品
        foreach ($result as &$r) {
            $isRepeat = false;
            foreach ($temp as &$t) {
                if($t['a_repository_id'] == $r['a_repository_id'] && $t['barcode'] == $r['barcode']){
                    $t['deliver_amount'] += $r['deliver_amount'];
                    $isRepeat = true;
                    break;
                }
            }
            if(!$isRepeat){
                array_push($temp, $r);
            }
        }
        $result = $temp;
        $Model = M('statistic_brepository_deliver');
        $Model->startTrans();
        foreach ($result as &$deliver) {
            $r = $Model->add($deliver);
            if(!$r){
                break;
            }
        }
        if(!$r){
            $Model->rollback();
            echo "false";
        }else{
            $Model->commit();
            echo json_encode($result);
        }
    }

    //所有仓库需要补货以及需要补货数量的数据(不按仓库分，只按类别分)
    public function getAllRepositorySupply(){
        $Model = M('product');
        $result = $Model->field('product.product_template_id, product_template.name, product_template.barcode, product.kj_price, (product.origin_amount-product.default_amount) as supply_amount')
                    ->join('product_template on product.product_template_id=product_template.product_template_id')
                    ->where('product.default_amount<product.origin_amount')
                    ->select();
        //遍历取出的结果，把相同barcode的商品汇总
        $temp = array(); //存储不重复的商品
        foreach ($result as &$r) {
            $isRepeat = false;
            foreach ($temp as &$t) {
                if($t['barcode'] == $r['barcode']){
                    $t['supply_amount'] += $r['supply_amount'];
                    $isRepeat = true;
                    break;
                }
            }
            if(!$isRepeat){
                array_push($temp, $r);
            }
        }
        $result = $temp;
        $Model = M('statistic_allrepository_supply');
        $Model->startTrans();
        foreach ($result as &$supply) {
            $r = $Model->add($supply);
            if(!$r){
                break;
            }
        }
        if(!$r){
            $Model->rollback();
            echo "false";
        }else{
            $Model->commit();
            echo json_encode($result);
        }
    }
}