<?php
namespace Admin\Controller;
use Think\Controller;
class StatisticController extends Controller {
    public function _initialize(){
        ini_set("memory_limit", "-1");
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }

    //入库报表（不明确：创建算入库？）
    public function inRepository(){
        $Model = D('admin_repository');
        $r = $Model->relation('admin')->select();
        $this->assign('data', json_encode($r));
        $this->display('Statistic/in-repository');
    }
    
    //出库报表（不明确：什么叫出库？用户下订单也算出库？）
    public function outRepository(){
        $Model = D('admin_repository');
        $r = $Model->relation('admin')->select();
        $this->assign('data', json_encode($r));
        $this->display('Statistic/out-repository');
    }

    public function abRepositorySupply(){
        $this->display('Statistic/ab-repository-supply');
    }

    public function allRepositorySupply(){
        $this->display('Statistic/all-repository-supply');
    }

    public function getabRepositorySupply(){
        //仓库管理员只可以看到自己所属的仓库的订单列表
        if(session('admin')['a_repository_id'] != null && session('admin')['a_repository_id'] != '0'){
            $param['product.a_repository_id'] = session('admin')['a_repository_id'];
        }else if(session('admin')['b_repository_id'] != null && session('admin')['b_repository_id'] != '0'){
            $param['product.b_repository_id'] = session('admin')['b_repository_id'];
        }
        
        //仓库管理员不能看到AB仓库补货统计报表
        $r = '';
        $r["draw"] = I("param.draw");
        $start = intval(I("param.start"))+1;
        $length = I("param.length");
        if(session('admin')['type'] != '0'){
            $Model = M('statistic_abrepository_supply');
            $r["data"] = $Model
                ->field('statistic_abrepository_supply.*, product_template.class1, product_template.class2, product_template.class3, product.a_repository_id, product.b_repository_id')
                ->join('product on statistic_abrepository_supply.product_id=product.product_id')
                ->join('product_template on product.product_template_id=product_template.product_template_id')
                ->join('product_class on product_template.class3=product_class.product_class_id')
                ->where($param)
                ->group('product_template.class1, product_template.class2, product_template.class3, statistic_abrepository_supply.product_id, statistic_abrepository_supply.createtime')
                ->order('product_class.index desc')
                ->select();
            $r["recordsTotal"] = count($r["data"]);
            $r["recordsFiltered"] = count($r["data"]);
            // $r["data"] = $Model
            //     ->field('statistic_abrepository_supply.*, product_template.class1, product_template.class2, product_template.class3, product.a_repository_id, product.b_repository_id')
            //     ->join('product on statistic_abrepository_supply.product_id=product.product_id')
            //     ->join('product_template on product.product_template_id=product_template.product_template_id')
            //     ->join('product_class on product_template.class3=product_class.product_class_id')
            //     ->where($param)
            //     ->group('product_template.class1, product_template.class2, product_template.class3, statistic_abrepository_supply.product_id, statistic_abrepository_supply.createtime')
            //     ->order('product_class.index desc')
            //     ->limit($start, $length)
            //     ->select();
        }
        echo json_encode($r);
    }

    //A仓库销售统计报表
    public function abRepositorySale(){
        //仓库管理员只可以看到自己所属的仓库的订单列表
        if(session('admin')['a_repository_id'] != null && session('admin')['a_repository_id'] != '0'){
            $param['product.a_repository_id'] = session('admin')['a_repository_id'];
        }else if(session('admin')['b_repository_id'] != null && session('admin')['b_repository_id'] != '0'){
            $param['product.b_repository_id'] = session('admin')['b_repository_id'];
        }

        $Model = M('statistic_abrepository_sale');
        $r = $Model
            ->field('statistic_abrepository_sale.*, product.a_repository_id, product.b_repository_id')
            ->join('product on statistic_abrepository_sale.product_id=product.product_id')
            ->where($param)
            ->select();
        $this->assign('data', json_encode($r));
        $this->display('Statistic/ab-repository-sale');
    }

    //B仓库送货单
    public function bRepositoryDeliver(){
        //仓库管理员只可以看到自己所属的仓库的订单列表
        if(session('admin')['a_repository_id'] != null && session('admin')['a_repository_id'] != '0'){
            $param['a_repository_id'] = session('admin')['a_repository_id'];
        }else if(session('admin')['b_repository_id'] != null && session('admin')['b_repository_id'] != '0'){
            $param['b_repository_id'] = session('admin')['b_repository_id'];
        }

        $Model = M('statistic_brepository_deliver');
        $r = $Model->where($param)->select();
        $this->assign('data', json_encode($r));
        $this->display('Statistic/b-repository-deliver');
    }
    
    //总仓库补货统计报表
    public function getAllRepositorySupply(){
        //仓库管理员不能看到商品分类
        $r = '';
        if(session('admin')['type'] != '0'){
            $Model = M('statistic_allrepository_supply');
            $r["data"] = $Model
                ->field('statistic_allrepository_supply.*, product_template.class1, product_template.class2, product_template.class3')
                ->join('product_template on statistic_allrepository_supply.product_template_id=product_template.product_template_id')
                ->join('product_class on product_template.class3=product_class.product_class_id')
                ->group('product_template.class1, product_template.class2, product_template.class3, statistic_allrepository_supply.product_template_id, statistic_allrepository_supply.createtime')
                ->order('product_class.index desc')
                ->select();
        }
        echo json_encode($r);
    }

}