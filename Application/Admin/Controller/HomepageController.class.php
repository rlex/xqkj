<?php
namespace Admin\Controller;
use Think\Controller;
class HomepageController extends Controller {
    public function _initialize(){
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }
    
    public function homepagePicture(){
       $this->display('Homepage/picture');
    }

    public function homepageAdvertisement(){
    	$this->display('Homepage/advertisement');
    }

    public function feedback(){
        $this->display('Homepage/feedback');
    }

    public function getHomepagePictrue(){
        $param['a_repository_id'] = I('param.a_repository_id');
        $param['position'] = I('param.position');
        $Model = M('home_picture');
        $r = $Model->where($param)->find();
        echo json_encode($r);
    }

    public function updateHomePagePicture(){
    	$param['a_repository_id'] = I('param.a_repository_id');
    	$param['position'] = I('param.position');
        $data = I('param.');
        $Model = M('home_picture');
    	$r = $Model->where($param)->find();
        if($r){
            if($r['imgurl'] != $data['imgurl']){
                unlink(DOC_ROOT . '/../' . $r['imgurl']);
            }
            if($r['linktype'] == '2' && $r['content'] != $data['content']){
                unlink(DOC_ROOT . '/../' . $r['content']);
            }
            $r1 = $Model->where($param)->save($data);
        }else{
            $r1 = $Model->add($data);
        }
        echo json_encode($r);
    }

    public function getHomepageAdvertisement(){
        $param['a_repository_id'] = I('param.a_repository_id');
        $param['position'] = I('param.position');
        $Model = M('home_advertisement');
        $r = $Model->where($param)->find();
        echo json_encode($r);
    }

    public function updateHomePageAdvertisement(){
        $param['a_repository_id'] = I('param.a_repository_id');
        $param['position'] = I('param.position');
        $data = I('param.');
        $Model = M('home_advertisement');
        $r = $Model->where($param)->find();
        if($r){
            if($r['imgurl'] != $data['imgurl']){
                unlink(DOC_ROOT . '/../' . $r['imgurl']);
            }
            $r1 = $Model->where($param)->save($data);
        }else{
            $r1 = $Model->add($data);
        }
        echo json_encode($r);
    }

    public function getFeedback(){
        //仓库管理员不能看到意见反馈
        $r['data'] = '';
        if(session('admin')['type'] != '0'){
            $Model = D('feedback');
            $r['data'] = $Model->relation(true)->select();
        }
        echo json_encode($r);
    }

    public function updateFeedback(){
        $data = I('param.');
        $data['admin_id'] = session('admin')['admin_id'];
        $Model = M('feedback');
        $r = $Model->save($data);
        echo json_encode($r);
    }

    //文件上传
    public function upload(){
        saveFile('homepage');
    }
}