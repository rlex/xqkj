<?php
namespace Admin\Controller;
use Admin\Model\ARepositoryModel;
use Admin\Model\BRepository;
use Admin\Model\BRepositoryModel;
use Admin\Model\DistrictModel;
use Think\Controller;
use Think\Model;
class DistrictController extends Controller {
    public function _initialize(){
        if(!isset($_SESSION['admin'])) {
            $this->display('Index/login');
            exit;
        }
    }

    public function districtDetail(){
		$this->display('District/district-detail');
    }

    public function districtList(){
    	$this->display('District/district-list');
    }

    public function notificationList(){
        $this->display('District/notification-list');
    }

    public function getDistrict(){
        $param['district_id'] = I('param.district_id', '');

        $Model = new Model();
        if($param['district_id'] == ''){
            $r['data'] = $Model
                ->table("district d, b_repository b, a_repository a")
                ->field("a.a_repository_id, b.b_repository_id, d.district_id, d.name, d.region, d.address, d.index")
                ->where("d.a_repository_id = a.a_repository_id and a.b_repository_id = b.b_repository_id")
                ->distinct(true)
                ->select();
        }else{
            $r = $Model
                ->table("district d, b_repository b, a_repository a")
                ->field("a.a_repository_id, b.b_repository_id, d.district_id, d.name, d.region, d.address, d.index")
                ->where("d.a_repository_id = a.a_repository_id and a.b_repository_id = b.b_repository_id")
                ->where($param)
                ->distinct(true)
                ->find();
        }

        echo json_encode($r);
    }

    public function updateDistrict(){
        $data = I('param.');

        $Model = M('district');
        if($data['district_id'] != ''){
            $r = $Model->save($data);
        }else{
            $r = $Model->add($data);
        }
        echo json_encode($r);
    }

    public function deleteDistrict(){
        $param['district_id'] = I('param.district_id', '');
        $Model = M('district');
        $r = $Model->where($param)->delete();
        echo json_encode($r);
    }

    public function getDistrictTree(){
        $a = M('a_repository');
        $r1 = $a->field('b_repository_id, a_repository_id, name')->select();

        $b = M('b_repository');
        $r2 = $b->field('b_repository_id, name')->select();

        $d = M('district');
        $r3 = $d->field('a_repository_id, district_id, name')->select();

        $r = array_merge($r1, $r2, $r3);
        echo json_encode($r);
    }

    public function getNotification(){
        $data['district_notification_id'] = I('param.district_notification_id', '');
        $Model = D('DistrictNotification');
    
        if($data['district_notification_id'] == ''){
            $r['data'] = $Model->relation('district')->select();
        }else{
            $r = $Model->where($data)->find();
            $r['content'] = preg_replace('/<br\\s*?\/??>/i','',$r['content']);
            //$r['content'] = htmlspecialchars($r['content']);
        }
        echo json_encode($r);
    }

    public function updateNotification(){
        $param['district_notification_id'] = I('param.district_notification_id', '');
        $data = I('param.');
        $data['content'] = nl2br($data['content']);
        $Model = M('DistrictNotification');
    
        if($data['district_notification_id'] == ''){
            $r = $Model->add($data);
        }else{
            $r = $Model->where($param)->save($data);
        }
        echo json_encode($r);
    }

    public function deleteNotification(){
        $param['district_notification_id'] = I('param.district_notification_id', '');
        $Model = M('DistrictNotification');

        $r = $Model->where($param)->delete();
        echo json_encode($r);
    }

}