<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class AdminRepositoryModel extends RelationModel
{
    protected $tableName = "admin_repository";

    protected $_link = array(
        'admin' => array(
            'mapping_type' => self::BELONGS_TO,
            'mapping_name' => 'admin',
            'foreign_key'  => 'admin_id',
            'mapping_fields'=> 'name'
        )

    );

}