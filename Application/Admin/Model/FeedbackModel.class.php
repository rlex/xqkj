<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class FeedbackModel extends RelationModel
{

    protected $tableName = "feedback";

    protected  $_link = array(
        'user' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'user_id',
            'mapping_name' => 'user',
            'mapping_fields'=> array('name','phone')
        ),

        'admin' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'admin_id',
            'mapping_name' => 'admin',
            'mapping_fields'=> 'name'
        ),
    );


}