<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class DistrictModel extends RelationModel
{
    protected $tableName = "district";

    protected  $_link = array(
        'a_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'a_repository_id',
            'mapping_name' => 'a_repository',

        )
    );


    public static function a_repository_for($id){
        $districtModel = new DistrictModel();
        return $districtModel->relation("a_repository")->find($id);
    }

    public static function b_repository_for($id){
        $districtModel = new DistrictModel();
        return $districtModel->relation(true)->find($id);
    }

}