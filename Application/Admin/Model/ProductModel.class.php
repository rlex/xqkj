<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class ProductModel extends RelationModel
{

    protected $tableName = "product";

		protected  $_link = array(
        'product_template' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'product_template_id',
            'mapping_name' => 'product_template',

        ),

        'a_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'a_repository_id',
            'mapping_name' => 'a_repository',

        ),

        'b_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'b_repository_id',
            'mapping_name' => 'b_repository',

        )
    );
}