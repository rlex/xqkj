<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class ProductTemplateModel extends RelationModel
{

    protected $tableName = "product_template";

    protected  $_link = array(
        'product' => array(
            'mapping_type' => self::HAS_MANY,
            'class_name'    => 'Product',
            'foreign_key'  => 'product_template_id',
            'mapping_name' => 'product'
        )
    );
}