<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class AdminOrderModel extends RelationModel
{
    protected $tableName = "admin_order";

		protected $_link = array(
        'admin' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name' => 'Admin',
            'mapping_name' => 'admin',
            'foreign_key'  => 'admin_id'
        ),

    );
}