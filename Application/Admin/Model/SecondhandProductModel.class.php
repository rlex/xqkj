<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class SecondhandProductModel extends RelationModel
{
    protected $tableName = "secondhand_product";

    protected $_link = array(
        'user' => array(
            'mapping_type' => self::BELONGS_TO,
            'mapping_name' => 'user',
            'foreign_key'  => 'user_id',
            'mapping_fields'=> 'name'
        ),

    );

}