<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class AdminCreditModel extends RelationModel
{
    protected $tableName = "admin_credit";

    protected $_link = array(
        'admin' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name' => 'admin',
            'mapping_name' => 'admin',
            'foreign_key'  => 'admin_id',
            'mapping_fields'=> 'name'
        )

    );

}