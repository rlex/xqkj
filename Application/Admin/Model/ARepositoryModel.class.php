<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class ARepositoryModel extends RelationModel
{

    protected $tableName = "a_repository";

    protected  $_link = array(
        'b_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'b_repository_id',
            'mapping_name' => 'b_repository',

        )
    );

    public static function b_repository_for($id){
        return (new ARepositoryModel())->relation(true)->find($id);
    }
}
