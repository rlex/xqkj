<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class SecondhandCommentModel extends RelationModel
{
    protected $tableName = "secondhand_comment";

    protected $_link = array(
        'from_user' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name'   => 'user',
            'mapping_name' => 'from_user',
            'foreign_key'  => 'from_user_id',
            'mapping_fields'=> 'name, user_id'
        ),

        'to_user' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name'   => 'user',
            'mapping_name' => 'to_user',
            'foreign_key'  => 'to_user_id',
            'mapping_fields'=> 'name, user_id'
        ),

        'secondhand_product' => array(
            'mapping_type' => self::BELONGS_TO,
            'mapping_name' => 'secondhand_product',
            'foreign_key'  => 'secondhand_product_id',
            'mapping_fields'=> 'a_repository_id, b_repository_id'
        ),

    );

}