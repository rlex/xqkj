<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class DistrictNotificationModel extends RelationModel
{
    protected $tableName = "district_notification";

		protected $_link = array(
        'district' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name' => 'District',
            'mapping_name' => 'district',
            'foreign_key'  => 'district_id'
        ),

    );
}