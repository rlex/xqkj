<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class ProductDiscountModel extends RelationModel
{

    protected $tableName = "product_discount";

    protected  $_link = array(
        'product' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'product_id',
            'mapping_name' => 'product',
			'class_name'   => 'Product'
        )
    );


}