<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class ProductCommentModel extends RelationModel
{

    protected $tableName = "product_comment";

		protected  $_link = array(
        'user' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'user_id',
            'mapping_name' => 'user'
        ),

        'product' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'product_id',
            'mapping_name' => 'product'
        ),
    );
}