<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class AdminModel extends RelationModel
{
    protected $tableName = "admin";

    protected $_link = array(
        'a_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name' => 'ARepository',
            'mapping_name' => 'a_repository',
            'foreign_key'  => 'a_repository_id',
            'mapping_fields'=> 'name'
        ),

        'b_repository' => array(
            'mapping_type' => self::BELONGS_TO,
            'class_name' => 'BRepository',
            'mapping_name' => 'b_repository',
            'foreign_key'  => 'b_repository_id',
            'mapping_fields'=> 'name'
        ),

    );

}