<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class OrderModel extends RelationModel
{
    protected $tableName = "order";

    protected $_link = array(

        'products' => array(
            'mapping_type' => self::HAS_MANY,
            'class_name' => 'OrderProduct',
            'mapping_name' => 'products',
            'foreign_key'  => 'order_id',
        ),

        'user' => array(
            'mapping_type' => self::BELONGS_TO,
            'mapping_name' => 'user',
            'foreign_key'  => 'user_id',
        ),

        'log' => array(
            'mapping_type' => self::HAS_MANY,
            'class_name' => 'AdminOrder',
            'mapping_name' => 'log',
            'foreign_key'  => 'order_id',
            'mapping_order' => 'createtime desc',
        ),

        'admin' => array(
            'mapping_type'      =>  self::MANY_TO_MANY,
            'class_name'        =>  'Admin',
            'mapping_name'      =>  'admin',
            'foreign_key'       =>  'order_id',
            'relation_foreign_key'  =>  'admin_id',
            'relation_table'    =>  'admin_order',
            'mapping_order' => 'a.createtime desc',
        )


    );
}