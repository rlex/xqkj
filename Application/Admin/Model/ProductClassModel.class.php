<?php

namespace Admin\Model;
use Think\Model\RelationModel;

class ProductClassModel extends RelationModel
{

    protected $tableName = "product_class";

    protected  $_link = array(
        'product_class' => array(
            'mapping_type' => self::BELONGS_TO,
            'foreign_key'  => 'parent_id',
            'mapping_name' => 'parent_class',

        )
    );

}